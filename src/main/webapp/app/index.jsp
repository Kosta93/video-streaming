<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title></title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width">

	<link rel="stylesheet" href="styles/main.css">
	<link rel="stylesheet" href="styles/vg-quality.css">
	<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.css">
</head>

<body ng-app="videoServerApp">

<div class="container">
	<div ng-view></div>
</div>
<div ng-controller="notification.ctrl as notificationCtrl">
	<notification messages="dm.messages"></notification>
</div>

<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular-sanitize.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.2.2/angular-route.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular-resource.min.js"></script>
<script src="http://static.videogular.com/scripts/videogular/latest/videogular.js"></script>
<script src="http://static.videogular.com/scripts/controls/latest/vg-controls.js"></script>
<script src="http://static.videogular.com/scripts/buffering/latest/vg-buffering.js"></script>
<script src="http://static.videogular.com/scripts/overlay-play/latest/vg-overlay-play.js"></script>
<script src="bower_components/videogular/vg-quality.js"></script>
<script src="bower_components/videogular/text-track.js"></script>


<script src="scripts/app.js"></script>
<script src="scripts/router.js"></script>
<!-- constants -->
<script src="constants/uri.cnst.js"></script>
<!-- configurations -->
<script src="scripts/configs/HTTPInterceptor.config.js"></script>
<!-- services -->
<script src="scripts/services/users.srvc.js"></script>
<script src="scripts/services/HRHttpInterceptors.srvc.js"></script>
<script src="scripts/services/authorization.srvc.js"></script>
<script src="scripts/services/videoToken.srvc.js"></script>
<!-- controllers -->
<script src="scripts/controllers/video.ctrl.js"></script>
<script src="scripts/controllers/login.ctrl.js"></script>
<script src="scripts/controllers/token.ctrl.js"></script>

    <!-- components -->
<script src="scripts/components/notifications/notification.ctrl.js"></script>
<script src="scripts/components/notifications/notification.drct.js"></script>
<script src="scripts/components/notifications/notification.srvc.js"></script>
</body>
</html>
