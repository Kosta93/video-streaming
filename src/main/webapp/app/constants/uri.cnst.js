/**
 * Created by Kosta on 10-Nov-15.
 */
'use strict';

(function () {
    angular
        .module('videoServerApp')
        .constant('uri.cnst', uriCnst());

    uriCnst.$inject = [];

    function uriCnst() {
        return  {
            BACKEND_URI: 'http://localhost:9966'
        };

    }
})();
