(function () {
	'use strict';

	angular
		.module('videoServerApp',
		[
			"ngSanitize",
			"ngRoute",
			"com.2fdevs.videogular",
			"com.2fdevs.videogular.plugins.controls",
			"com.2fdevs.videogular.plugins.buffering",
			"com.2fdevs.videogular.plugins.overlayplay",
			"th.co.softever.videogular.plugins.quality",
			"videogular.plugins.texttrack",
			"ngResource"
		])
		.run(onAppStart);

	onAppStart.$inject = ['$rootScope', '$location', 'authorization.srvc'];

	function onAppStart($rootScope, $location, authorizationSrvc) {
		console.debug("app started");

		$location.path("/login");

		$rootScope.$on('$routeChangeStart', function (event, toState, toParams, fromState, fromParams) {
			var authorized = authorizationSrvc.authorizedFor(toState);
			if (authorized == authorizationSrvc.authorization.REQUIRES_LOGIN) {
				//$location.path('/login');
			}
			else if (authorized == authorizationSrvc.authorization.UNAUTHORIZED) {
				// TODO: display error page or something
			}
		});
	}
})();
