/**
 * Created by Kosta on 25-Dec-15.
 */

'use strict';

(function () {
	angular
		.module('videoServerApp')
		.factory('HRHttpInterceptors.srvc', HRHttpInterceptors);

	HRHttpInterceptors.$inject = ['$q', '$location', '$rootScope'];

	function HRHttpInterceptors($q, $location, $rootScope) {

		var service = {
			request: requestInterceptor,
			responseError: responseError
		};

		return service;

		///////////////////////////////

		function requestInterceptor(config) {
			// Add token in each request header
			var authToken = $rootScope.authToken;
			if (angular.isDefined(authToken)) {
				config.headers['X-Auth-Token'] = authToken;
				console.log(authToken);
			}
			return config || $q.when(config);
		}

		function responseError(rejection) {
			var status = rejection.status;
			if (status == 400) {
				// invalid data

			}
			if (status == 401) {
				console.log(rejection.statusText);
				$location.path("/login");
			}
			if (status == 404) {
				console.log("nepostoecko url");
			}
			return $q.reject(rejection);
		}
	}
})();