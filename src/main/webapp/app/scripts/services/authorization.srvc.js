/**
 * Created by Kosta on 26-Dec-15.
 */

'use strict';

(function () {
	angular
		.module('videoServerApp')
		.factory('authorization.srvc', authorizationSrvc);

	authorizationSrvc.$inject = ['$rootScope'];

	function authorizationSrvc($rootScope) {

		/**
		 * Result enum of the authorization process
		 */
		var authorization = {
			AUTHORIZED: 0,
			UNAUTHORIZED: 1,
			REQUIRES_LOGIN: 2
		};

		return {
			authorizedFor: authorizedFor,
			authorization: authorization
		};


		//////////////////////////////

		/**
		 * Checks if the current user is authorized for the state(route) he wants to access
		 */
		function authorizedFor(state) {
			if (!state.access) {
				return authorization.AUTHORIZED;
			}
			var currentUser = $rootScope.user;
			if (state.access.requiresLogin && !currentUser) {
				return authorization.REQUIRES_LOGIN;
			}
			return authorization.AUTHORIZED;
		}
	}
})();