/**
 * Created by Kosta on 09-Nov-15.
 */
'use strict';

(function () {
    angular
	    .module('videoServerApp')
	    .factory('users.srvc', usersService);

    usersService.$inject = ['$resource', 'uri.cnst'];

    function usersService($resource, uriCnst) {
        return  $resource(uriCnst.BACKEND_URI + '/rest/user/:action', {}, {
            authenticate: {
                method: 'POST',
                params: {
                    'action': 'authenticate'
                },
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }
        });
    }
})();