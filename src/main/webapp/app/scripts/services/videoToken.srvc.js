/**
 * Created by Kosta on 26-Dec-15.
 */

'use strict';

(function () {
	angular
		.module('videoServerApp')
		.factory('videoToken.srvc', videoTokenSrvc);

	videoTokenSrvc.$inject = ['$http', 'uri.cnst'];

	function videoTokenSrvc($http, uriCnst) {
		var token;

		return {
			getLocalToken: getLocalToken,
			fetchToken: fetchToken
		};


		//////////////////////////////


		function getLocalToken() {
			if (angular.isDefined(token)) {
				return token;
			}
			else {
				console.log("Token is not defined");
			}
		}

		function fetchToken(videoName) {
			return $http({
				method: 'GET',
				url: uriCnst.BACKEND_URI + '/test/token/' + videoName,
				headers: {
					/*'Access-Control-Allow-Origin': '*'*/
				}
			}).then(successCallback, errorCallback);
		}

		function successCallback(response) {
			console.log(response.data);
			token = response.data;
		}

		function errorCallback(response) {
			console.log(response);
		}
	}
})();