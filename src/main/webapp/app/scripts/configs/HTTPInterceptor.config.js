/**
 * Created by Kosta on 25-Dec-15.
 */

'use strict';

(function () {
	angular
		.module('videoServerApp')
		.config(['$httpProvider', function ($httpProvider) {
			$httpProvider.interceptors.push('HRHttpInterceptors.srvc');
		}]);
})();