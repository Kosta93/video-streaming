'use strict';

(function() {
	angular
		.module('videoServerApp')
		.directive('notification', notification);

	notification.$inject = [];

	function notification() {
		return {
			restrict: "E",
			scope: {
				messages: "="
			},
			templateUrl: 'scripts/components/notifications/notification.tmpl.html'
		};
	}
})();
