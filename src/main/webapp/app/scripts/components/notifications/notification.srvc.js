'use strict';

(function() {
	angular
		.module('videoServerApp')
		.factory('notification.srvc', notificationSrvc);

	notificationSrvc.$inject = ['$rootScope'];

	function notificationSrvc($rootScope) {
		var service = {
				danger: danger,
				info: info,
				success: success,
				warning: warning,
				onMessage: onMessage,
				onRemoveMessage: onRemoveMessage,
				removeMessage: removeMessage
			},
			messageId = 1,
			generatedId;

		return service;

		////////////////////////////////////////////////


		function onMessage(callback) {
			$rootScope.$on('notificationSrvc.incommingMessageEvent', function(eventData, message) {
				callback(message);
			});
		}

		function onRemoveMessage(callback) {
			$rootScope.$on('notificationSrvc.removeMessageEvent', function(eventData, id) {
				callback(id);
			});
		}

		function removeMessage(id) {
			$rootScope.$broadcast('notificationSrvc.removeMessageEvent', id);
		}

		function messageIdGenerator() {
			return messageId++;

		}

		function warning(message) {
			return messageCreator(message, 'warning');
		}

		function info(message) {
			return messageCreator(message, 'info');
		}

		function success(message) {
			return messageCreator(message, 'success');
		}

		function danger(message) {
			return messageCreator(message, 'danger');
		}

		function messageCreator(messageContent, messageType) {
			generatedId = messageIdGenerator();
			$rootScope.$broadcast('notificationSrvc.incommingMessageEvent', {
				type: messageType,
				content: messageContent,
				id: generatedId
			});
			return generatedId;
		}
	}
})();
