'use strict';


(function() {
	angular
		.module('videoServerApp')
		.controller('notification.ctrl', notificationCtrl);

	notificationCtrl.$inject = ['$scope', '$timeout', 'notification.srvc'];

	function notificationCtrl($scope, $timeout, notificationSrvc) {
		var timeout = 5000;

		$scope.dm = {
			messages: {
				info: {},
				warning: {},
				danger: {},
				success: {}
			}
		};
		notificationSrvc.onMessage(addMessage);
		notificationSrvc.onRemoveMessage(removeMessage);

		function addMessage(newMessage) {
			$scope.dm.messages[newMessage.type][newMessage.id] = {
				content: newMessage.content
			};
			if (newMessage.type === 'info' || newMessage.type === 'success') {
				messageTimeout(newMessage.id);
			}
		}

		function messageTimeout(id) {
			$timeout(function() {
				removeMessage(id);
			}, timeout);
		}

		function removeMessage(id) {
			for (var type in $scope.dm.messages) {
				if ($scope.dm.messages[type][id]) {
					delete $scope.dm.messages[type][id];
					break;
				}
			}
		}
	}
})();
