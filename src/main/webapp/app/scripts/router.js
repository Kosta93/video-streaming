(function () {
	'use strict';

	angular
		.module('videoServerApp')
		.config(config);

	config.$inject = ['$routeProvider'];

	function config($routeProvider) {
		$routeProvider.when('/', {
			templateUrl: 'views/login.html',
			controller: 'login.ctrl',
			controllerAs: 'vm'
		});

		$routeProvider.when('/video', {
			templateUrl: 'views/video.html',
			controller: 'video.ctrl',
			controllerAs: 'vm',
			access: {
				requiresLogin: true
			}
		});

		$routeProvider.when('/getToken', {
			templateUrl: 'views/token.html',
			controller: 'token.ctrl',
			controllerAs: 'vm'
		});

		$routeProvider.otherwise({
			redirectTo: '/'
		});
	}
})();