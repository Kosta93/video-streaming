/**
 * Created by Kosta on 26-Dec-15.
 */

'use strict';

(function () {
	angular
		.module('videoServerApp')
		.controller('token.ctrl', tokenCtrl);

	tokenCtrl.$inject = ['videoToken.srvc'];

	function tokenCtrl(videoTokenSrvc) {
		var vm = this;

		angular.extend(vm, {
			fetchToken: fetchToken
		});

		function fetchToken(videoName) {
			videoTokenSrvc.fetchToken(videoName);
		}
	}
})();
