/**
 * Created by Kosta on 02-Nov-15.
 */

(function () {
	'use strict';

	angular
		.module('videoServerApp')
		.controller('login.ctrl', LoginController);

	LoginController.$inject = ['$scope', '$rootScope', '$location', 'users.srvc', 'notification.srvc'];

	function LoginController($scope, $rootScope, $location, usersSrvc, notificationSrvc) {
		var vm = this;
		var errorsId = [];

		angular.extend(vm, {
			login: login
		});

		function login() {
			usersSrvc.authenticate($.param({
					username: $scope.username,
					password: $scope.password
				}), function (authenticationResult) {
					$rootScope.authToken = authenticationResult.token;
					$rootScope.user = authenticationResult.user;
					$rootScope.loginAction = true;
					$location.path("/video");
				},
				function (error) {
					errorsId[0] = notificationSrvc.info("Something happened! ");
				});
		}
	}
})
();
