(function () {
	'use strict';

	angular
		.module('videoServerApp')
		.controller('video.ctrl', VideoController);

	VideoController.$inject = ['$sce', '$rootScope', 'videoToken.srvc'];

	function VideoController($sce, $rootScope, videoTokenSrvc) {
		var vm = this;

		vm.videoToken = function () {
			return videoTokenSrvc.getLocalToken();
		};

		vm.API = null;

		vm.config = {
			preload: "none",
			qualitySources: [
				{
					name: '720p',
					sources: [
						{src: $sce.trustAsResourceUrl("http://localhost:9966/test/preview/SampleVideo720"
							+ '?token=' + $rootScope.authToken
							+ '&videoToken=' + vm.videoToken()), type: "video/mp4"}
					]
				},
				{
					name: '360p',
					sources: [
						{src: $sce.trustAsResourceUrl("http://localhost:9966/test/preview/SampleVideo360"
							+ '?token=' + $rootScope.authToken
							+ '&videoToken=' + vm.videoToken()), type: "video/mp4"}
					]
				}
			],
			tracks: [
				{
					src: $sce.trustAsResourceUrl("assets/subs/SampleSubtitleEnglish.vtt"),
					kind: "subtitles",
					srclang: "en",
					label: "English",
					default: "true"
				}
			],
			theme: {
				url: "http://www.videogular.com/styles/themes/default/latest/videogular.css"
			},
			plugins: {
				controls: {
					autoHide: true,
					autoHideTime: 5000
				},
				subtitle: [
					{
						kind: "captions",
						src: $sce.trustAsResourceUrl("assets/subs/SampleSubtitleEnglish.vtt"),
						srclang: "en",
						label: "English"
					}
				]
			}
		};
		this.currentQualitySource = this.config.qualitySources[0];
	}
})();
