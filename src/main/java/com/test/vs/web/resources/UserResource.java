package com.test.vs.web.resources;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.test.vs.model.User;
import com.test.vs.security.TokenTransfer;
import com.test.vs.security.TokenUtils;
import com.test.vs.security.UserTransfer;
import com.test.vs.service.UserService;

@RestController
@RequestMapping("/rest/user")
public class UserResource {

	@Autowired
	private UserDetailsService userDetailsService;

	@Autowired
	@Qualifier("authenticationManager")
	private AuthenticationManager authManager;

	@Autowired
	private UserService userService;

	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public UserTransfer getUser(HttpServletRequest request, HttpServletResponse response) throws IOException {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		Object principal = authentication.getPrincipal();
		if (principal instanceof String && ((String) principal).equals("anonymousUser")) {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
		}
		if (principal instanceof UserDetails) {

			UserDetails userDetails = (UserDetails) principal;
			User user = userService.findByUsername(userDetails.getUsername());
			return new UserTransfer(user.getUsername(), user.getRole().toString());
		}
		return null;
	}

	@RequestMapping(value = "/authenticate", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public TokenTransfer authenticate(@RequestParam("username") String username,
			@RequestParam("password") String password,
			HttpServletRequest request, HttpServletResponse response) {

		UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username,
				password);
		Authentication authentication = this.authManager.authenticate(authenticationToken);
		SecurityContextHolder.getContext().setAuthentication(authentication);

		/*
		 * Reload user as password of authentication principal will be null
		 * after authorization and password is needed for token generation
		 */
		UserDetails userDetails = this.userDetailsService.loadUserByUsername(username);

		User user = userService.findByUsername(username);

		return new TokenTransfer(TokenUtils.createToken(userDetails), user);
	}
}
