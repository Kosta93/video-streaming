package com.test.vs.web;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.activation.MimeType;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.MediaType;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.test.vs.security.VideoTokenUtils;
import com.test.vs.service.impl.MultipartFileSender;

@RestController
@RequestMapping("/test")
public class TestController {

	private String filepath = "C:\\Users\\Kosta\\Desktop\\";

	@RequestMapping(value = "/preview/{fileName}", method = RequestMethod.GET, produces = MediaType.MULTIPART_FORM_DATA_VALUE)
	public void getVideo(@PathVariable String fileName, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String requestedFile = filepath + fileName + ".mp4";
		Path path = Paths.get(requestedFile);
		MultipartFileSender.fromPath(path).with(request).with(response).serveResource();

	}

	@RequestMapping(value = "/token/{videoName}", method = RequestMethod.GET, produces = "application/json")
	public String getVideoToken(@PathVariable String videoName, HttpServletRequest request,
			HttpServletResponse response) {
		return VideoTokenUtils.createToken(videoName);
	}

	@RequestMapping(value = "/subtitle/{fileName}", method = RequestMethod.GET)
	@ResponseBody
	public void getSubtitle(@PathVariable String fileName, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		response.setContentType("text/plain");
		String requestedFile = filepath + fileName + ".vtt";
		FileInputStream inputStream = new FileInputStream(requestedFile);
		try {
			String everything = IOUtils.toString(inputStream);
			response.getWriter().append(everything);
			response.flushBuffer();
		} finally {
			inputStream.close();
		}
	}

}