package com.test.vs.repository;

import com.test.vs.model.User;

public interface UserRepository extends JpaSpecificationRepository<User>{

	User findByUsername(String username);
}
