package com.test.vs.security;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerMapping;

public class VideoTokenProcessingFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		System.out.println("FILTER INITIATED");
		HttpServletRequest httpRequest = this.getAsHttpRequest(request);
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		String videoToken = this.extractAuthTokenFromRequest(httpRequest);
		String ETag = httpRequest.getServletPath().replace("/test/preview/", "");
		ETag = ETag.substring(0, ETag.length()-3);
		if (videoToken == null) {
			httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "NOT AUTHORIZED");
		} else if (VideoTokenUtils.validateToken(videoToken, ETag)) {
			System.out.println("TOKEN IS VALID!");
			chain.doFilter(request, response);
		} else {
			System.out.println("TOKEN IS NOT VALID!");
			httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "NOT AUTHORIZED");
		}
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	private HttpServletRequest getAsHttpRequest(ServletRequest request) {
		if (!(request instanceof HttpServletRequest)) {
			throw new RuntimeException("Expecting an HTTP request");
		}
		return (HttpServletRequest) request;
	}

	private String extractAuthTokenFromRequest(HttpServletRequest httpRequest) {
		/* Get token from request parameter */
		return httpRequest.getParameter("videoToken");
	}
}
