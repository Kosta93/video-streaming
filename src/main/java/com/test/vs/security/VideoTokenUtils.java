package com.test.vs.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.springframework.security.crypto.codec.Hex;

public class VideoTokenUtils {
	public static final String MAGIC_KEY = "obfuscate";

	public static String createToken(String videoName) {
		/* Expires in one minute */
		long expires = System.currentTimeMillis() + 1000L * 60;
		StringBuilder tokenBuilder = new StringBuilder();
		tokenBuilder.append(videoName);
		tokenBuilder.append(":");
		tokenBuilder.append(expires);
		tokenBuilder.append(":");
		tokenBuilder.append(VideoTokenUtils.computeSignature(videoName, expires));

		return tokenBuilder.toString();
	}

	public static String computeSignature(String ETag, long expires) {
		StringBuilder signatureBuilder = new StringBuilder();
		signatureBuilder.append(ETag);
		signatureBuilder.append(":");
		signatureBuilder.append(expires);
		signatureBuilder.append(":");
		signatureBuilder.append(TokenUtils.MAGIC_KEY);
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			throw new IllegalStateException("No MD5 algorithm available!");
		}

		String out = new String(Hex.encode(digest.digest(signatureBuilder.toString().getBytes())));
		return out;
	}

	public static String getVideoNameFromToken(String videoToken) {
		if (null == videoToken) {
			return null;
		}
		String[] parts = videoToken.split(":");
		return parts[0];
	}

	public static boolean validateToken(String videoToken, String ETag) {
		String[] parts = videoToken.split(":");
		if (parts.length!=3) return false;
		long expires = Long.parseLong(parts[1]);
		String signature = parts[2];
		if (expires < System.currentTimeMillis()) {
			System.out.println("VIDEO TOKEN HAS EXPIRED");
			return false;
		}

		return signature.equals(VideoTokenUtils.computeSignature(ETag, expires));
	}
}
