package com.test.vs.service;

import com.test.vs.model.User;

public interface UserService extends BaseEntityCrudService<User> {

	User findByUsername(String username);
}
