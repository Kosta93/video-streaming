package com.test.vs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.vs.model.User;
import com.test.vs.repository.UserRepository;
import com.test.vs.service.UserService;

@Service
public class UserServiceImpl extends BaseEntityCrudServiceImpl<User, UserRepository> implements UserService {

	@Autowired
	private UserRepository repository;

	@Override
	protected UserRepository getRepository() {
		return repository;
	}

	@Override
	public User findByUsername(String username) {
		return repository.findByUsername(username);
	}
}
